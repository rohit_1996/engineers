package com.Companydetails.Engineers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EngineersController {

	@Autowired
	EngineersService ens;
	
	@PostMapping("/howareyou")
	@ResponseBody
	public EngineersRecords saverecords(@RequestBody EngineersRecords erd) {
		return ens.saveengineer(erd);
	}
	
}
