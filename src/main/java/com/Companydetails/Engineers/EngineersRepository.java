package com.Companydetails.Engineers;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EngineersRepository extends JpaRepository<EngineersRecords, Integer> {
	
	

}
