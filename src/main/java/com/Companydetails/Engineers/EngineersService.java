package com.Companydetails.Engineers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EngineersService {
	
	@Autowired
	EngineersRepository er;
	
	public EngineersRecords saveengineer(EngineersRecords erc) {
		return er.save(erc);
	}

}
